import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';


import { SearchComponent } from '../components/search/search.component';
import { AboutComponent } from '../components/about/about.component';
import { ArtistComponent } from '../components/artist/artist.component';
import { AlbumComponent } from '../components/album/album.component';

export const appRoutes: Routes = [
  {path: '', component: SearchComponent},
  {path: 'About', component: AboutComponent},
  {path: 'artist/:id', component: ArtistComponent},
  {path: 'album/:id', component: AlbumComponent},
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
