import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';


import { SpotifyService } from '../../services/spotify.service';
import { Artist } from '../../interface/artist';
import { Album } from '../../interface/album';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {

  id: string;
  album: Album[];

  constructor(private _spotifyService: SpotifyService, private _route: ActivatedRoute) { }

  ngOnInit() {
    this._route.params
    .subscribe((params: Params) => {
      this._spotifyService.getAlbumsingle(params['id'])
      .subscribe((album: Album[]) => {
        this.album = album;
      });
    });
  }

}
