import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';


import { SpotifyService } from '../../services/spotify.service';
import { Artist } from '../../interface/artist';
import { Album } from '../../interface/album';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css']
})
export class ArtistComponent implements OnInit {

  id: string;
  artist: Artist[];
  albums: Album[];

  constructor(private _spotifyService: SpotifyService, private _route: ActivatedRoute) { }

  ngOnInit() {
    this._route.params.subscribe((params: Params) => {
      this._spotifyService.getArtist(params['id'])
      .subscribe((artist: Artist[]) => {
        this.artist = artist;
      });
      this._spotifyService.getAlbum(params['id'])
      .subscribe((album: Album[]) => {
        this.albums = album.items;
      });
    });
  }

}
