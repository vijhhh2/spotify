import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';
import { Artist } from '../../interface/artist';



@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  searchStr: string;
  searchRes: Artist[];
  constructor(private spotify: SpotifyService) { }

  ngOnInit() {
  }

  searchMusic() {

    this.spotify.searchMusic(this.searchStr)
    .subscribe((res) => {
      this.searchRes = res['artists'].items;
    });
  }

}
