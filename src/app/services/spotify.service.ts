import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class SpotifyService {
  private searchUrl: string;
  private artistUrl: string;
  private albumUrl: string;
  private albumUrlSingle: string;
  private clientId = '6d795d358c3c40b2bf69b470eb58d36b';
  private clientSecret = '33bd1b6ec8604ec58f3892a90ee94e76';
  constructor(private _http: HttpClient) { }

  searchMusic(str: string, type= 'artist') {
    this.searchUrl = `https://api.spotify.com/v1/search?q=${str}&type=${type}&offset=20&limit=20`;
    return this._http.get(this.searchUrl);
  }
  getArtist(id: string) {
    this.artistUrl = `https://api.spotify.com/v1/artists/${id}`;
    return this._http.get(this.artistUrl);
  }
  getAlbum(id: string) {
    this.albumUrl = `https://api.spotify.com/v1/artists/${id}/albums`;
    return this._http.get(this.albumUrl);
  }
  getAlbumsingle(id: string) {
    this.albumUrlSingle = `https://api.spotify.com/v1/albums/${id}`;
    return this._http.get(this.albumUrlSingle);
  }

}
