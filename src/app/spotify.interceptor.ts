import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpRequest, HTTP_INTERCEPTORS, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()

export class SpotifyAuthInterceptor implements HttpInterceptor {
  // tslint:disable-next-line:max-line-length
  private token = 'BQCDmIODQGPZIwILLuyJOatiVLzBwGFXoMiiSFP087BY-fTMXsa1h51jZ3_1jq13L3v9ygMq6kuwj65erCGH4E4QcCPrHApxdDc6n9wAfX9tW1Byp8k0Xx8oPkfQlg5jPkm_LUa9i8KSL7m5mk04gKQS9fZN9STI&refresh_token=AQDz7JkSAreVymzbbGRPOEvIEHAE6PZUM6-fFO5m_1OBd-51hJEHM1EIGpyY_s_LSXb8al8RNRm8B36QfE5DhZaR9QxNC2mOJuoMPzJr02-LKdCT63Ulgugvvt78Q3PKugI';
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const authReq = req.clone({
      headers: req.headers.set('Authorization', 'Bearer ' + this.token)
    });

    return next.handle(authReq);

  }

}
